package com.summit.springbootinaction.chap2;

import com.summit.springbootinaction.chap3.Reader;
import com.summit.springbootinaction.chap3.ReaderRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@Controller
public class ReadingListController {
    private ReadingListRepository readingListRepository;
    private ReaderRepository readerRepository;
    private AmazonProperties amazonConfig;

    @Autowired
    public ReadingListController(ReadingListRepository readingListRepository,
                                 ReaderRepository readerRepository,
                                 AmazonProperties amazonConfig) {
        this.readingListRepository = readingListRepository;
        this.readerRepository = readerRepository;
        this.amazonConfig = amazonConfig;
    }

    @GetMapping("/readingList")
    public String readerBooks( Reader reader, Model model) {
        log.info("readingList get method is called");
        List<Reader> readerList = readerRepository.findAllBy();

        List<Book> readingList = readingListRepository.findByReader(readerList.get(0));
        if (readingList != null) {
            model.addAttribute("books", readingList);
            model.addAttribute("reader", reader);
            model.addAttribute("amazonID", amazonConfig.getAssociateId());
        }
        return "readingList";
    }

    @PostMapping("/readingList")
    public String addToReadingList(Reader reader, Book book) {
        List<Reader> readerList = readerRepository.findAllBy();
        reader = readerList.get(0);
        book.setReader(reader);
        readingListRepository.save(book);
        return "redirect:/readingList";
    }

    @ExceptionHandler(value = RuntimeException.class)
    @ResponseStatus(value = HttpStatus.BANDWIDTH_LIMIT_EXCEEDED)
    public String error() {
        return "error";
    }
}
