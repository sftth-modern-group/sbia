package com.summit.springbootinaction.chap3;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ReaderRepository extends JpaRepository<Reader, String> {
    List<Reader> findAllBy();
}
