insert into Reader (username, password, fullname) values ('craig', 'password', 'Craig Walls');
insert into Reader (username, password, fullname) values ('walt', 'marceline', 'Walt Disney');

insert into User (username, password, fullname) values ('craig', '{Bcrypt}$2a$10$lGuL0rIBBhsM.FYMF34k1e2T5.75CCtjQXc56pk5id8f4Oy2..QeS', 'Craig Walls');
insert into User (username, password, fullname) values ('walt', '{Bcrypt}$2a$10$R0AA.zN3AVHj8TlXybPvkeb1C.kzi0zBkj/yHpyGMks.m.QuEnQxa', 'Walt Disney');



